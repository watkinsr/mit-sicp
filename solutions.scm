(define (factorial n)
  (if (= n 1)
      1
      (* n (factorial (- n 1)))))

(define (count-change amount coins)
  (cond ((= amount 0) 1)
	((or (< amount 0) (= coins 0)) 0)
	(else (+ (count-change amount (- coins 1))
		 (count-change (- amount (denomination coins)) coins)))))

(define (denomination coins)
  (cond ((= coins 1) 1)
	((= coins 2) 5)
	((= coins 3) 10)
	((= coins 4) 25)
	((= coins 5) 50)))

(count-change 100 5)
(count-change 10 2)

(define (ex-1.11 n)
  (if (< n 3) n
      (+
       (+ (ex-1.11 (- n 1)) (* 2 (ex-1.11 (- n 2))))
       (* 3 (ex-1.11 (- n 3)))) ))

(define (f n) 
  ;; Track previous three values. 
  ;; fi-1 is f(i-1) 
  ;; fi-2 is f(i-2) 
  ;; fi-3 is f(i-3) 
  (define (f-iter fi-1 fi-2 fi-3 i) 
    ;; Calculate value at current index i. 
    (define fi (+ fi-1 
		  (* 2 fi-2) 
		  (* 3 fi-3))) 
    (if (= i n) 
	fi 
	(f-iter fi fi-1 fi-2 (+ i 1)))) 
  
  (if (< n 3) 
      n 
      (f-iter 2 1 0 3))) ;; start index i=3, count up until reach n. 

(ex-1.11 3) ;; (+ (+ 2 2) 0)
(f 4)

(define (pascals-triangle n)
  (define (iter i xs)
    (if (> i n) xs
	(iter (+ i 1) (get-line xs i))))
  (iter 1 '()))

(define (get-line prev i)
  (if (null? prev) '(1)
      (let loop
	  ((c 0)
	   (xs ())
	   (prev-line (list-ref prev (- i 2))))
	;; (display prev)
	(if
	 (= c i) (append prev (cons xs '()))
	 (loop (+ c 1) (cons (get-element c i prev-line) xs) prev-line)))))

(define (get-element i n prev)
  (cond ((= i 0) 1)
	((= i (- n 1)) 1)
	(else (+
	       (list-ref prev (- i 1))
	       (list-ref prev i)))))

(pascals-triangle 2) ;; (1 (1 1))
